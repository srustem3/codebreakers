package columns;

import general.Decryptor;

import java.util.*;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class Decoder implements Decryptor {
    private static List<List<Character>> _columnsAfterKey;
    private static Byte[] _key;

    public String decode(String source, Byte[] key) throws Exception {
        List<List<Character>> columns = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            columns.add(new ArrayList<>());
        }
        for (int i = 0; i < columns.size(); i++) {
            for (int j = (source.length() * (i)) / key.length; j < (source.length() * (i+1)) / key.length; j++) {
                if (source.charAt(j) == '.') {
                    columns.get(i).add(' ');
                } else {
                    columns.get(i).add(source.charAt(j));
                }
            }
        }

        List<List<Character>> columnsAfterKey = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            columnsAfterKey.add(columns.get(key[i]));
        }

        int counter = 0;
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < columnsAfterKey.get(0).size(); i++) {
            for (int j = 0; j < key.length; j++) {
                if (columnsAfterKey.get(j).size() > i) {
                    sb.append(columnsAfterKey.get(j).get(i));
                    counter++;
                }
            }
            if (counter > 100) {
                break;
            }
        }
        _key = key;
        _columnsAfterKey = columnsAfterKey;

        return sb.toString();
    }

    @Override
    public String prepareForOutput() {
        StringBuilder res = new StringBuilder("");
        for (int i = 0; i < _columnsAfterKey.get(0).size(); i++) {
            for (int j = 0; j < _key.length; j++) {
                if (_columnsAfterKey.get(j).size() > i) {
                    res.append(_columnsAfterKey.get(j).get(i));
                }
            }
        }
        return res.toString();
    }
}