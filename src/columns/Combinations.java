package columns;

import java.util.Stack;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
// Перебирает все возможные перестановки
public class Combinations {
    private static final byte USED = Byte.MAX_VALUE;
    public static void printCombination(byte[] arr, Stack<Byte> s, int am) {
        if (am == 0) {
            Object[] obj = s.toArray();
            Byte[] bes = new Byte[obj.length];
            for (int i = 0; i < obj.length; i++) {
                bes[i] = (byte)((Byte) obj[i] - 1);
            }
            Main.attempts.add(bes);
            return;
        }
        for (int i = 0; i < arr.length; i++) if (arr[i] != USED) {
            s.push(arr[i]);
            arr[i] = USED;
            printCombination(arr, s, am - 1);
            arr[i] = s.pop();
        }
    }
    public static void main(String[] args) {
        printCombination(new byte[]{1,2,3,4}, new Stack<>(), 4);
    }
}