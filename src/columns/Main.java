package columns;

import check.CheckWord;
import check.DataBase;
import general.InputOutput;

import java.util.*;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class Main {
    public static List<Byte[]> attempts;

    public static void main(String[] args) throws Exception {
        String source = InputOutput.read("Teams/We/GreenTeamN.txt");
        StringBuilder newSource = new StringBuilder("");
        for (int i = 0; i < source.length(); i++) {
            newSource.append(source.charAt(source.length() - 1 - i));
        }
        DataBase.connectionToDB();
        Decoder decoder = new Decoder();
        for (int i = 2; i < 7; i++) {
            byte[] acceptable = new byte[i];
            for (int j = 0; j < acceptable.length; j++) {
                acceptable[j] = (byte)(j + 1);
            }
            attempts = new LinkedList<>();
            Combinations.printCombination(acceptable, new Stack<>(), i);
            for (int j = 0; j < attempts.size(); j++) {
                String newLine = decoder.decode(newSource.toString(), attempts.get(j));
                CheckWord.checkWordList(newLine, decoder);
            }
        }
        DataBase.disconnectionFromDB();
    }
}
