package columns;

import general.InputOutput;

import java.util.*;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class Encoder {
    public static void main(String[] args) throws Exception {
        byte[] key = {3, 0, 4, 2, 1};
        String source = InputOutput.read("src/secret.txt");
        
        List<List<Character>> columns = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            columns.add(new LinkedList<>());
        }
        for (int i = 0; i < source.length(); i++) {
            columns.get(i % key.length).add(source.charAt(i));
        }

        int q = columns.size() - 1;
        while (columns.get(q).size() < columns.get(0).size()) {
            columns.get(q).add('.');
            q--;
        }

        Map<Integer, List<Character>> map = new HashMap<>();
        for (int i = 0; i < key.length; i++) {
            map.put((int)key[i], columns.get(i));
        }

        StringBuilder res = new StringBuilder("");
        map.forEach((k, v) -> res.append(v.stream().map(i -> i + "").reduce("", (acc, el) -> acc + el)));
        System.out.println(res);
        // Reverse
        StringBuilder resReverse = new StringBuilder("");
        for (int i = 0; i < res.length(); i++) {
            resReverse.append(res.charAt(res.length() - i - 1));
        }
        InputOutput.printDecoded(resReverse.toString());

        // Обычная перестановка букв по ключу в пределах строк
        /*for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).getValue().size(); j++) {
                res.append(list.get(i).getValue().get(j));
            }
        }*/
    }
}
