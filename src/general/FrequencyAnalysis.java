package general;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class FrequencyAnalysis {
    public static void main(String[] args) throws Exception {
        Map<String, Integer> symbols = new HashMap<>();
         String source = InputOutput.read("src/Shifr.txt");
        for (int i = 0; i < source.length(); i += 1) {
            String trigram = source.substring(i, i + 1);
            if (symbols.containsKey(trigram)) {
                int preValue = symbols.get(trigram);
                symbols.put(trigram, preValue + 1);
            } else {
                symbols.put(trigram, 1);
            }
        }
        System.out.println(symbols.size());
        symbols = symbols.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
        symbols.forEach((k, v) -> System.out.println(k + "  " + v));
    }
}
