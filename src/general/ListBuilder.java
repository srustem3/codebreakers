package general;

import java.util.Scanner;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class ListBuilder {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        String temp = sc.nextLine();
        StringBuilder res = new StringBuilder("");
        res.append("alph.add(\"" + temp + "\");\n");
        while (!temp.equals("()")) {
            temp = sc.nextLine();
            res.append("alph.add(\'" + temp + "\');\n");
        }
        System.out.println(res.toString());
    }
}
