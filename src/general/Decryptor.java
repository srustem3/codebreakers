package general;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public interface Decryptor {
    String prepareForOutput();
}
