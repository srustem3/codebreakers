package general;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class InputOutput {
    public static String read(String path) throws Exception {
        Charset charset = Charset.forName("UTF-8");
        CharsetDecoder decoder = charset.newDecoder();

        FileChannel fc = new FileInputStream(path).getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(20000); // При необходимости размер подправить ручками
        fc.read(buffer);
        buffer.flip();
        CharBuffer charBuffer = decoder.decode(buffer);
        StringBuilder stringBuilder = new StringBuilder("");
        while (charBuffer.hasRemaining()) {
            stringBuilder.append(charBuffer.get());
        }
        return stringBuilder.toString();
    }

    public static void printDecoded(String decoded) throws Exception {
        FileChannel fc = new FileOutputStream("src/out.txt").getChannel();
        CharBuffer charBufferOut = CharBuffer.allocate(10000); // При необходимости размер подправить ручками

        for (int i = 0; i < decoded.length(); i++) {
            charBufferOut.put(decoded.charAt(i));
        }

        Charset charset = Charset.forName("UTF-8");
        CharsetEncoder encoder = charset.newEncoder();
        charBufferOut.flip();
        ByteBuffer bufferOut = encoder.encode(charBufferOut);
        fc.write(bufferOut);
        fc.close();
    }
}
