package check;

import general.Decryptor;
import general.InputOutput;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class CheckWord {
    public static Statement st;
    public static HashSet<String> stopList = new HashSet<>();

    public static void checkWordList(String decrypted, Decryptor decryptor) {
        //System.out.println(decrypted); Если нужно каждый раз смотреть расшифрованнный вариант, раскомментировать
        String[] words = decrypted.split("[^А-Яа-я]+");
        List<String> wordList;
        wordList = Arrays.stream(words).filter(el -> el.length() >= 3).map(String::toLowerCase).collect(Collectors.toList());
        int amount = 2;
        int count = 0;
        for (int j = 0; j < wordList.size(); j++) {
            int in;
            boolean result = checkWord(wordList.get(j));
            if (result && count < amount) {
                count++;
                continue;
            }
            if (result) {
                System.out.println("==================================");
                System.out.println(decryptor.prepareForOutput());
                System.out.println("------- Нашли слово: " + wordList.get(j));
                Scanner sc = new Scanner(System.in);
                System.out.println("1 - ОК, 2 - продолжить перебор");
                in = sc.nextInt();
                if (in == 1) {
                    String decRes = decryptor.prepareForOutput();
                    try {
                        InputOutput.printDecoded(decRes);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                        System.out.println(decRes);
                    }
                    DataBase.disconnectionFromDB();
                    System.exit(0);
                }
                stopList.add(wordList.get(j));
                break;
            }
        }
    }

    public static boolean checkWord(String word) {
        ResultSet rs = null;
        try {
            rs = st.executeQuery("select count(*) from dictionary where word = '" + word + "'");
            while (rs.next()) {
                if (rs.getInt(1) > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        return false;
    }
}
