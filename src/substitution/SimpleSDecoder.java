package substitution;

import check.CheckWord;
import general.Decryptor;

import java.util.ArrayList;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class SimpleSDecoder implements Decryptor {
    private StringBuilder attempt = new StringBuilder("");

    public void run(String source) {
        ArrayList<Character> alph = new ArrayList<>();
        alph.add('а');
        alph.add('б');
        alph.add('в');
        alph.add('г');
        alph.add('д');
        alph.add('е');
        alph.add('ж');
        alph.add('з');
        alph.add('и');
        alph.add('й');
        alph.add('к');
        alph.add('л');
        alph.add('м');
        alph.add('н');
        alph.add('о');
        alph.add('п');
        alph.add('р');
        alph.add('с');
        alph.add('т');
        alph.add('у');
        alph.add('ф');
        alph.add('х');
        alph.add('ц');
        alph.add('ч');
        alph.add('ш');
        alph.add('щ');
        alph.add('ъ');
        alph.add('ы');
        alph.add('ь');
        alph.add('э');
        alph.add('ю');
        alph.add('я');
        ArrayList<Character> na;
        for (int i = 29; i >= 0; i--) {
            for (int j = 28; j >= 0; j--) {
                for (int k = 27; k >= 0; k--) {
                    na = new ArrayList<>(alph);
                    na.set(30, alph.get(i));
                    na.set(i, 'ю');
                    na.set(29, alph.get(j));
                    na.set(j, 'э');
                    na.set(28, alph.get(k));
                    na.set(k, 'ь');
                    for (int m = 0; m < source.length(); m++) {
                        char c = source.charAt(m);
                        if (c >= 'а' && c <= 'я') {
                            int index = alph.indexOf(c);
                            attempt.append(na.get(index));
                        } else {
                            attempt.append(c);
                        }
                    }
                    CheckWord.checkWordList(attempt.toString(), this);
                    attempt = new StringBuilder("");
                }
            }
        }
    }

    @Override
    public String prepareForOutput() {
        return attempt.toString();
    }
}
