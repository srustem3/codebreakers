package substitution.caesar;

import check.CheckWord;
import general.Decryptor;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class CaesarDecoder implements Decryptor {
    String attempt;

    public void run(String source) {
        for (int i = 6; i < 7; i += 1) {
            int key = i;
            attempt = source.chars()
                    .mapToObj(j -> {
                        if ('а' <= j && j <= 'я') {
                            if (j - key < 'а') {
                                int n = key - (j - 'а');
                                int tmpKey = n - (n / 32) * 32;
                                return (char) ('я' - tmpKey + 1) + "";
                            } else {
                                return (char)(j - key) + "";
                            }
                        }
                        else if ('А' <= j && j <= 'Я') {
                            if (j - key < 'А') {
                                int n = key - (j - 'А');
                                int tmpKey = n - (n / 32) * 32;
                                return (char) ('Я' - tmpKey + 1) + "";
                            }
                            else {
                                return (char)(j - key) + "";
                            }
                        } else {
                            return (char)(j) + "";
                        }
                    })
                    .reduce("", (acc, symbol) -> acc + symbol);
            CheckWord.checkWordList(attempt, this);
        }
    }

    @Override
    public String prepareForOutput() {
        return attempt;
    }
}