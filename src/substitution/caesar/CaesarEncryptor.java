package substitution.caesar;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class CaesarEncryptor {
    public String encrypt(String text, int key) {
        return text.chars()
                .mapToObj(i -> {
                    if ('а' <= i && i <= 'я') {
                        if (key + i > 'я') {
                            int n = i + key - 'я' - 1;
                            int tmpKey = n - (n / 32) * 32;
                            return (char) ('а' + tmpKey) + "";
                        }
                        else {
                            return (char)(i + key) + "";
                        }
                    }
                    else {
                        if (('А' <= i && i <= 'Я')) {
                            if (key + i > 'Я') {
                                int n = i + key - 'Я' - 1;
                                int tmpKey = n - (n / 32) * 32;
                                return (char) ('А' + tmpKey) + "";
                            } else {
                                return (char) (i + key) + "";
                            }
                        }
                    }
                    return (char) i + "";
                })
                .reduce("", (acc, symbol) -> acc + symbol);
    }
}


