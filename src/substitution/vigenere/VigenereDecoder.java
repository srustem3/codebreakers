package substitution.vigenere;

import check.CheckWord;
import check.DataBase;
import general.Decryptor;
import general.InputOutput;

import java.util.Arrays;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class VigenereDecoder implements Decryptor {
    StringBuilder decrypted = new StringBuilder("");
    byte[] _key;

    public static void main(String[] args) throws Exception {
        DataBase.connectionToDB();
        String source = InputOutput.read("src/Encrypted_book_by_Blue_team.txt");
        VigenereDecoder cd = new VigenereDecoder();
        cd.run(source);
    }

    // Переборщик
    public void run(String source) {
        /*byte[] x2 = {0, 0};
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 32; j++) {
                x2[0] = (byte)i;
                x2[1] = (byte)j;
                try {
                    decrypt(source, x2);
                } catch (Exception ex) {

                }
            }
        }*/
        /*boolean broken = false;
        byte[] x3 = {0, 0, 0};
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 32; j++) {
                if (broken) {
                    broken = false;
                    break;
                }
                for (int k = 0; k < 32; k++) {
                    x3[0] = (byte)i;
                    x3[1] = (byte)j;
                    x3[2] = (byte)k;
                    try {
                        decrypt(source, x3);
                    } catch (FoundUnacceptableFirstSymbolException ex) {
                        System.out.println("----------- " + ex.getMessage() + " " + i + " " + j + " " + k);
                        broken = true;
                        break;
                    }
                }
            }
        }*/
        byte[] x3 = {0, 0, 0};
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 32; j++) {
                for (int k = 0; k < 32; k++) {
                    x3[0] = (byte)i;
                    x3[1] = (byte)j;
                    x3[2] = (byte)k;
                    try {
                        decrypt(source, x3);
                    } catch (FoundUnacceptableFirstSymbolException ex) {

                    }
                }
            }
            System.out.println(i);
        }
        /*byte[] x4 = {0, 0, 0, 0};
        for (int i = 0; i < 33; i++) {
            for (int j = 0; j < 33; j++) {
                for (int k = 0; k < 33; k++) {
                    for (int l = 0; l < 33; l++) {
                        x4[0] = (byte)i;
                        x4[1] = (byte)j;
                        x4[2] = (byte)k;
                        x4[3] = (byte)l;
                        decrypt(source, x4);
                    }
                }
            }
            System.out.println(Arrays.toString(x4));
        }*/

        /*byte[] x5 = {0, 0, 0, 0, 0};
        for (int i = 10; i < 20; i++) {
            for (int j = 10; j < 20; j++) {
                for (int k = 10; k < 20; k++) {
                    for (int l = 10; l < 20; l++) {
                        for (int m = 10; m < 20; m++) {
                            x5[0] = (byte)i;
                            x5[1] = (byte)j;
                            x5[2] = (byte)k;
                            x5[3] = (byte)l;
                            x5[4] = (byte)m;
                            decrypt(source, x5);
                        }
                    }
                }
            }
            System.out.println(Arrays.toString(x5));
        }

        byte[] x6 = {0, 0, 0, 0, 0, 0};
        for (int i = 10; i < 20; i++) {
            for (int j = 10; j < 20; j++) {
                for (int k = 10; k < 20; k++) {
                    for (int l = 10; l < 20; l++) {
                        for (int m = 10; m < 20; m++) {
                            for (int n = 10; n < 20; n++) {
                                x6[0] = (byte)i;
                                x6[1] = (byte)j;
                                x6[2] = (byte)k;
                                x6[3] = (byte)l;
                                x6[4] = (byte)m;
                                x6[5] = (byte)n;
                                decrypt(source, x6);
                            }
                        }
                    }
                }
            }
            System.out.println(Arrays.toString(x6));
        }*/
    }

    public void decrypt(String source, byte[] key) throws FoundUnacceptableFirstSymbolException {
        //System.out.println(Arrays.toString(key)); // Если хотим вывести ключ, раскомментировать
        // Если нужно перебирать без учёта длины алфавита (можем вылететь за границы), то раскомментировать код ниже
            /*for (int i = 0; i < source.length(); i++) {
                decrypted.append((char)(source.charAt(i) + key[i % key.length]));
            }*/
        String[] strings = new String[source.length()];
        int i = 0;
        while (i != key.length) {
            for (int j = i; j < source.length(); j += key.length) {
                char letter = source.charAt(j);
                int newKey = key[i];
                if ('а' <= letter && letter <= 'я') {
                    if (letter - newKey < 'а') {
                        int n = newKey - (letter - 'а');
                        int tmpKey = n - (n / 32) * 32;
                        strings[j] = (char) ('я' - tmpKey + 1) + "";
                    } else {
                        strings[j] = (char) (letter - newKey) + "";
                    }
                } else {
                    if ('А' <= letter && letter <= 'Я') {
                        if (letter - newKey < 'А') {
                            int n = newKey - (letter - 'А');
                            int tmpKey = n - (n / 32) * 32;
                            strings[j] = (char) ('Я' - tmpKey + 1) + "";
                        } else {
                            strings[j] = (char) (letter - newKey) + "";
                        }
                    } else {
                        strings[j] = (char)(letter) + "";
                    }
                }
            }
            i++;
        }
        decrypted.append(Arrays.stream(strings).reduce("", (acc, symbol) -> acc + symbol));
        _key = key;
        CheckWord.checkWordList(decrypted.toString(), this);
        decrypted = new StringBuilder("");
    }

    @Override
    public String prepareForOutput() {
        return Arrays.toString(_key) + "\n" + decrypted.toString();
    }
}