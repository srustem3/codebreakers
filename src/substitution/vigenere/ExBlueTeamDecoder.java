package substitution.vigenere;

import check.CheckWord;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class ExBlueTeamDecoder extends VigenereDecoder {
    ArrayList<Character> alph = new ArrayList<>();
    int counter = 0; // Счётчик русских символов

    public ExBlueTeamDecoder() {
        alph.add('а');
        alph.add('б');
        alph.add('в');
        alph.add('г');
        alph.add('д');
        alph.add('е');
        alph.add('ж');
        alph.add('з');
        alph.add('и');
        alph.add('й');
        alph.add('к');
        alph.add('л');
        alph.add('м');
        alph.add('н');
        alph.add('о');
        alph.add('п');
        alph.add('р');
        alph.add('с');
        alph.add('т');
        alph.add('у');
        alph.add('ф');
        alph.add('х');
        alph.add('ц');
        alph.add('ч');
        alph.add('ш');
        alph.add('щ');
        alph.add('ъ');
        alph.add('ы');
        alph.add('ь');
        alph.add('э');
        alph.add('ю');
        alph.add('я');
    }

    byte[] _key;
    @Override
    public void decrypt(String source, byte[] key) throws FoundUnacceptableFirstSymbolException {
        _key = key;
        for (int i = 0; i < source.length(); i++) {
            char c = decodeSymbol(source.charAt(i), key[counter % key.length]);
            if (counter == 0 && (c == 'ы' || c == 'ъ' || c == 'ь')) {
                throw new FoundUnacceptableFirstSymbolException("");
            }
            if (rus) {
                counter++;
            } else {
                counter = 0;
            }
            decrypted.append(c);
        }
        counter = 0;
        CheckWord.checkWordList(decrypted.toString(), this);
        decrypted = new StringBuilder("");
    }

    boolean rus = false;

    // Не трогать!
    public char decodeSymbol(char s, byte b) {
        if (alph.indexOf(s) == -1) {
            counter = 0;
            rus = false;
            return s;
        } else {
            rus = true;
            int diff = alph.indexOf(s) - b;
            if (diff < 0) {
                return alph.get(alph.size() + diff);
            } else {
                return alph.get(diff);
            }
        }
    }

    public static void main(String[] args) {
        ExBlueTeamDecoder bt = new ExBlueTeamDecoder();
        for (int i = 0; i < 35; i++) {
            System.out.println(bt.decodeSymbol('-', (byte)i));
        }
    }

    @Override
    public String prepareForOutput() {
        return Arrays.toString(_key);
    }
}