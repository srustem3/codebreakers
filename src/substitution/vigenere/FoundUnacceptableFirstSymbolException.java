package substitution.vigenere;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
class FoundUnacceptableFirstSymbolException extends Exception {
    public FoundUnacceptableFirstSymbolException(String message) {
        super(message);
    }
}