package substitution.vigenere;

import general.InputOutput;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class KeyOffset {
    public static void main(String[] args) throws Exception {
        String source = InputOutput.read("");
        //source = source.substring(1); Если UTF-8 с BOM, раскомментировать
        StringBuilder s = new StringBuilder("");
        for (int i = 2; i < source.length(); i+= 3) { // Предполагаемая длина ключа == 3
            s.append(source.charAt(i));
        }
        InputOutput.printDecoded(s.toString());
    }
}
