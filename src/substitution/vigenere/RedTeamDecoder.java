package substitution.vigenere;

import check.CheckWord;
import check.DataBase;
import general.InputOutput;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class RedTeamDecoder extends VigenereDecoder {
    public static void main(String[] args) throws Exception {
        DataBase.connectionToDB();
        String source = InputOutput.read("");
        RedTeamDecoder vd = new RedTeamDecoder();
        source = source.substring(1).toLowerCase();
        byte[] key = {0, 1, 5, 2, 15, -1};
        for (int i = 0; i < 54; i++) {
            vd.decrypt(source, key);
            System.out.println(Arrays.toString(key));
            for (int j = 0; j < key.length; j++) {
                key[j] = (byte)(key[j] + 1);
            }
            System.out.println("==========================================================");
        }
        DataBase.disconnectionFromDB();
    }

    ArrayList<Character> alph = new ArrayList<>();

    public RedTeamDecoder() {
        alph.add(' ');
        alph.add('.');
        alph.add(',');
        alph.add('!');
        alph.add('?');
        alph.add('(');
        alph.add(')');
        alph.add('-');
        alph.add(';');
        alph.add(':');
        alph.add('\"');
        alph.add('0');
        alph.add('1');
        alph.add('2');
        alph.add('3');
        alph.add('4');
        alph.add('5');
        alph.add('6');
        alph.add('7');
        alph.add('8');
        alph.add('9');
        alph.add('а');
        alph.add('б');
        alph.add('в');
        alph.add('г');
        alph.add('д');
        alph.add('е');
        alph.add('ё');
        alph.add('ж');
        alph.add('з');
        alph.add('и');
        alph.add('й');
        alph.add('к');
        alph.add('л');
        alph.add('м');
        alph.add('н');
        alph.add('о');
        alph.add('п');
        alph.add('р');
        alph.add('с');
        alph.add('т');
        alph.add('у');
        alph.add('ф');
        alph.add('х');
        alph.add('ц');
        alph.add('ч');
        alph.add('ш');
        alph.add('щ');
        alph.add('ъ');
        alph.add('ы');
        alph.add('ь');
        alph.add('э');
        alph.add('ю');
        alph.add('я');
    }

    @Override
    public void decrypt(String source, byte[] key) {
        for (int i = 0; i < source.length(); i++) {
            int index = alph.indexOf(source.charAt(i));
            int newIndex;
            if (index - key[i % key.length] < 0) {
                newIndex = alph.size() + (index - key[i % key.length]);
            } else {
                newIndex = index - key[i % key.length];
            }
            decrypted.append(alph.get(newIndex));
        }
        CheckWord.checkWordList(decrypted.toString(), this);
        decrypted = new StringBuilder("");
    }
}
