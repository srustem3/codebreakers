package substitution.vigenere;

import general.InputOutput;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class FridmanTest {
    static char[] a;

    public static void main(String[] args) throws Exception {
        String source = InputOutput.read("src/in.txt");
        a = source.toCharArray();
        char[] b = new char[a.length];
        for (int i = 0; i < a.length; i++) {
            if (i+1 != a.length) {
                b[i + 1] = a[i];
            } else {
                b[0] = a[a.length - 1];
            }
        }
        // Выводятся циклические сдвиги от 2-х
        shift(shift(shift(shift(shift(shift(shift(shift(b))))))));
    }

    public static char[] shift(char[] b) {
        char temp = b[b.length - 1];
        for (int i = b.length - 1; i > 0; i--) {
            b[i] = b[i-1];
        }
        b[0] = temp;
        int coincidence = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] == b[i]) {
                coincidence++;
            }
        }
        System.out.println((coincidence + 0.0) / a.length);
        return b;
    }
}
